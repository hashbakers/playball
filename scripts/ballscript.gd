extends RigidBody2D


var points = 0
var multiplier = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func add_points(points):
	self.points += points*self.multiplier
	
func add_multiplier(multiplier):
	self.multiplier *= multiplier

func _on_collide(collider):
	if collider.is_in_group("Bouncer1"):
		var basevector = (self.position - collider.position)*25
		self.apply_central_impulse(basevector.round())
		self.add_points(500)
		print("points: ",self.points)
	elif collider.is_in_group("Bouncer2"):
		var basevector = (collider.get_node("./Node2D").global_position-collider.global_position)*500
		self.apply_central_impulse(basevector.round())
		self.add_points(1000)
		print("points: ",self.points)
